#ifndef MQ_H_
# define MQ_H_

#include "_kernel.h"

#define MQ_QUEUE_SIZE       1024
#define MQ_MSG_LIMIT        64*1024
#define MQ_KMEM_LIMIT       PAGE_SIZE*4
#define MQ_NODE             "mqueue"
#define MQ_PROC_FILE        "/proc/" MQ_NODE
#define MQ_PID_FILE         "/var/run/" MQ_NODE ".pid"

#pragma pack(1)

typedef struct _mq_data {
    size_t size;
    void *data;
} mq_data;

typedef struct _mq_transport {
    size_t size;
    char data[1];
} mq_transport;

#pragma pack()

void usage( const char *self, const char *msg );
int perr( const char *prefix, const char *msg );

#endif
