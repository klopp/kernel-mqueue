#include "_kernel.h"

#if LINUX_VERSION_CODE < KERNEL_VERSION(2,6,26)
struct proc_dir_entry *proc_create( const char *name,
                                    mode_t mode,
                                    struct proc_dir_entry *parent,
                                    struct file_operations *proc_fops ) {
    struct proc_dir_entry *pde = create_proc_entry( MQ_NODE,
                                 S_IFREG | S_IRUGO | S_IWUGO, parent );

    if( pde ) {
        pde->uid = pde->gid = 0;
        pde->data = NULL;
        pde->proc_fops = fops;
    }

    return pde;
}
#endif
