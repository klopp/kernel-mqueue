#include "mq.h"

/* -------------------------------------------------------------------------- */
static DECLARE_KFIFO( mq, mq_data, MQ_QUEUE_SIZE );
static struct proc_dir_entry *proc_entry;
static struct mutex mq_lock;
/*
 * ???
 */
static char *outfile = NULL;
module_param( outfile, charp, 0 );

/* -------------------------------------------------------------------------- */
static void mq_free( void *mqd ) {
    if( ( ( mq_data * )mqd )->size < MQ_KMEM_LIMIT ) {
        kfree( ( ( mq_data * )mqd )->data );
    }
    else {
        vfree( ( ( mq_data * )mqd )->data );
    }
}

/* -------------------------------------------------------------------------- */
static mq_data *mq_data_create( mq_data *mqd, void *data, size_t size ) {
    mqd->size = 0;
    mqd->data = NULL;

    if( size < MQ_KMEM_LIMIT ) {
        mqd->data = kmalloc( size, GFP_KERNEL );
    }
    else {
        mqd->data = vmalloc( size );
    }

    if( !mqd->data ) {
        return NULL;
    }

    mqd->size = size;
    copy_from_user( mqd->data, data, size );
    return mqd;
}

/* -----------------------------------------------------------------------------
 * Оно же - push_back()
 *
 * Логика возврата ошибок немного хромает. Если ошибка возникла при приёме
 * первого же сообщения - OK. А если нет, и часть сообщений успешно засосалась?
 * Может быть в этом случае имеет смысл возвращать размер обработанного
 * буфера, пусть посылающий разбирается.
 ---------------------------------------------------------------------------- */
static ssize_t mq_write( struct file *file, const char *buf,
                         size_t count, loff_t *ppos ) {
    mq_data mqd;
    ssize_t rc = count;
    ssize_t left = count;
    const char *mqt = buf;
    size_t dsize;

    while( !mutex_trylock( &mq_lock ) ) {
        cpu_relax();
    }

    while( left ) {
        if( kfifo_is_full( &mq ) ) {
            rc = -ERANGE;
            printk( KERN_ERR "MQ queue is full!\n" );
            break;
        }

        /*
         * Сообщение не может быть таким маленьким:
         */
        if( left < sizeof( size_t ) + 1 ) {
            rc = -EINVAL;
            printk( KERN_ERR "MQ message too small (%zu bytes)!\n", left );
            break;
        }

        dsize = ( ( mq_transport * )mqt )->size;

        /*
         * Сообщение не может быть таким большим:
         */
        if( dsize > MQ_MSG_LIMIT ) {
            rc = -EINVAL;
            printk( KERN_ERR "MQ message too big (%zu bytes)!\n", dsize );
            break;
        }

        if( !mq_data_create( &mqd, ( ( mq_transport * )mqt )->data, dsize ) ) {
            printk( KERN_ERR "MQ no memory for new message\n" );
            rc = -ENOMEM;
            break;
        }

        if( kfifo_in( &mq, &mqd, 1 ) != 1 ) {
            printk( KERN_ERR "MQ error inserting message\n" );
            rc = -EACCES;
            mq_free( &mqd );
            break;
        }

        printk( KERN_INFO "MQ message inserted OK (%zu bytes)\n", dsize );
        left -= dsize + sizeof( size_t );
        mqt += dsize + sizeof( size_t );
    }

    mutex_unlock( &mq_lock );
    return rc;
}


/* -----------------------------------------------------------------------------
 * Оно же - pop_front()
 -----------------------------------------------------------------------------*/
static ssize_t mq_read( struct file *file, char *buf,
                        size_t count, loff_t *ppos ) {
    mq_data mqd;
    ssize_t rc = 0;

    if( kfifo_is_empty( &mq ) ) {
        printk( KERN_ERR "MQ queue is empty\n" );
        return -ERANGE;
    }

    while( !mutex_trylock( &mq_lock ) ) {
        cpu_relax();
    }

    while( 1 ) {
        /*
         * Сначала - проверить хватает ли места в
         * пользовательском буфере для текущего элемента:
         */
        if( !kfifo_out_peek( &mq, &mqd, 1 ) ) {
            printk( KERN_ERR "MQ kfifo_out_peek() error\n" );
            rc = -ENOENT;
            break;
        }

        if( count < mqd.size + sizeof( size_t ) ) {
            printk( KERN_ERR "MQ too small buffer (%zu bytes, needed %zu)!\n", count,
                    mqd.size + sizeof( size_t ) );
            rc = -EINVAL;
            break;
        }

        /*
         * Хватило места - OK,читаем и перегоняем в юзерспейс:
         */
        if( !kfifo_out( &mq, &mqd, 1 ) ) {
            printk( KERN_ERR "MQ kfifo_out() error\n" );
            rc = -ENOENT;
            break;
        }

        copy_to_user( buf, &mqd.size, sizeof( size_t ) );
        copy_to_user( buf + sizeof( size_t ), mqd.data, mqd.size );
        rc += sizeof( size_t ) + mqd.size;
        printk( KERN_INFO "MQ message sent to user OK (%zu bytes)\n", mqd.size );
        mq_free( &mqd );
        break;
    }

    mutex_unlock( &mq_lock );
    return rc;
}

/* -------------------------------------------------------------------------- */
static const struct file_operations mq_fops = {
    .owner = THIS_MODULE,
    .read  = mq_read,
    .write  = mq_write
};

/* -------------------------------------------------------------------------- */
static int __init mq_init( void ) {
#if defined(USE_OUTFILE )

    if( !outfile ) {
        printk( KERN_ERR "No outfile parameter!\n" );
        return -EINVAL;
    }

#endif
    INIT_KFIFO( mq );
    proc_entry = proc_create( MQ_NODE, S_IFREG | S_IRUGO | S_IWUGO, NULL,
                              &mq_fops );

    if( !proc_entry ) {
        printk( KERN_ERR "Can not create proc/%s\n", MQ_NODE );
        return -ENOMEM;
    }

    mutex_init( &mq_lock );
    return 0;
}

/* -------------------------------------------------------------------------- */
static void __exit mq_exit( void ) {
    mq_data mqd;

    /*
     * Удаляем все элементы списка:
     */
    while( kfifo_out( &mq, &mqd, 1 ) ) {
        mq_free( &mqd );
    }

    proc_remove( proc_entry );
}

/* -------------------------------------------------------------------------- */
module_init( mq_init );
module_exit( mq_exit );
MODULE_LICENSE( "GPL" );
MODULE_AUTHOR( "Vsevolod Lutovinov <klopp@yandex.ru>" );

/* -------------------------------------------------------------------------- */
