#include "mq.h"

#define     ARG_N   "-n="

static const char usg[] = "message [-n=NODE_FILE] [message ...]";

int main( int argc, char **argv ) {
    int i = 1;
    int rc = EXIT_SUCCESS;
    int mq;
    char *procfile = MQ_PROC_FILE;
    size_t messages = 0, messages_sent = 0;

    if( argc < 2 ) {
        usage( argv[0], usg );
    }

    while( i < argc ) {
        if( !memcmp( argv[i], ARG_N, sizeof( ARG_N ) - 1 ) ) {
            procfile = argv[i] + sizeof( ARG_N ) - 1;

            if( !*procfile ) {
                usage( argv[0], usg );
            }
        }
        else {
            messages++;
        }

        i++;
    }

    if( !messages ) {
        usage( argv[0], usg );
    }

    mq = open( procfile, O_WRONLY );

    if( mq == -1 ) {
        return perr( "can not open node file", procfile );
    }

    i = 1;

    while( i < argc ) {
        if( !memcmp( argv[i], ARG_N, sizeof( ARG_N ) - 1 ) ) {
            i++;
            continue;
        }

        size_t mlen = strlen( argv[i] ) + 1;
        printf( "writing message \"%s\" (%zu bytes)...\n", argv[i],
                mlen + sizeof( size_t ) );
        mq_transport *mqt = malloc( mlen + sizeof( size_t ) );

        if( !mqt ) {
            perr( "can not allocate memory", NULL );
            break;
        }

        mqt->size = mlen;
        strcpy( mqt->data, argv[i] );

        if( write( mq, mqt, mlen + sizeof( size_t ) ) == -1 ) {
            perr( "can not write", procfile );
            break;
        }
        else {
            messages_sent++;
        }

        free( mqt );
        i++;
    }

    printf( "%zu from %zu messages sent.\n", messages_sent, messages );
    close( mq );
    return rc;
}

