
#define _BSD_SOURCE
#define _XOPEN_SOURCE
#include "mq.h"
#include "pidfile.h"

/*
 * Use getopt()?
 */
#define KEY_S           "-s="
#define KEY_O           "-o="
#define KEY_P           "-p="
#define KEY_N           "-n="
#define SLEEP_SECONDS   60

static const char usg[] =
    "-o=OUTPUT_FILE [-n=NODE_FILE] [-p=PID_FILE] [-s=SLEEP_SECONDS]";

static int terminate = 0;

/*
 * Common signal handler:
 */
static void mq_sig_handler( int signo ) {
    switch( signo ) {
        case SIGTERM:
        case SIGINT:
            syslog( LOG_ERR, "TERM/INT received, terminating..." );
            terminate = 1;
            break;

        default:
            syslog( LOG_ERR, "Any signal received, stop waiting..." );
            break;
    }
}

/*
 *
 */
static void mq_sig_init( int signo ) {
    struct sigaction sa;
    memset( &sa, 0, sizeof( struct sigaction ) );
    sa.sa_handler = mq_sig_handler;
    sigfillset( &sa.sa_mask );
    sigaction( signo, &sa, NULL );
}

/*
 *
 */
int main( int argc, char **argv ) {
    int i = 1;
    int rc = EXIT_SUCCESS;
    sigset_t sig_set;
    int mq;
    unsigned long sleep_seconds = SLEEP_SECONDS;
    char *outfile = "/tmp/mqueue.bin"/*NULL*/;
    char *pidfile = MQ_PID_FILE;
    char *procfile = MQ_PROC_FILE;
    mq_transport *mqt = NULL;

    while( i < argc ) {
        if( !memcmp( argv[i], KEY_S, sizeof( KEY_S ) - 1 ) ) {
            char *endptr;
            sleep_seconds = strtoul( argv[i] + sizeof( KEY_S ) - 1, &endptr, 10 );

            if( *endptr || sleep_seconds > UINT_MAX || sleep_seconds < 1 ) {
                usage( argv[0], usg );
            }
        }
        else if( !memcmp( argv[i], KEY_O, sizeof( KEY_O ) - 1 ) ) {
            outfile = argv[i] + sizeof( KEY_O ) - 1;

            if( !*outfile ) {
                usage( argv[0], usg );
            }
        }
        else if( !memcmp( argv[i], KEY_P, sizeof( KEY_P ) - 1 ) ) {
            pidfile = argv[i] + sizeof( KEY_P ) - 1;

            if( !*pidfile ) {
                usage( argv[0], usg );
            }
        }
        else if( !memcmp( argv[i], KEY_P, sizeof( KEY_N ) - 1 ) ) {
            procfile = argv[i] + sizeof( KEY_N ) - 1;

            if( !*procfile ) {
                usage( argv[0], usg );
            }
        }
        else {
            usage( argv[0], usg );
        }

        i++;
    }

    if( !outfile ) {
        usage( argv[0], usg );
    }

    if( check_pid( pidfile ) ) {
        fprintf( stderr, "\nERROR, daemon already loaded!\n" );
        return EADDRINUSE;
    }

    mq = open( procfile, O_RDONLY );

    if( mq == -1 ) {
        return perr( "can not open", procfile );
    }

    mqt = malloc( sizeof( size_t ) + MQ_MSG_LIMIT );

    if( !mqt ) {
        rc = perr( "can not allocate message transport memory", NULL );
        close( mq );
        return rc;
    }

    if( daemon( 0, 0 ) ) {
        rc = perr( "can not fork", NULL );
        close( mq );
        free( mqt );
        return rc;
    }

    openlog( NULL, LOG_NDELAY | LOG_PID, LOG_USER );

    if( !write_pid( pidfile ) ) {
        rc = errno;
        syslog( LOG_ERR, "ERROR, can not create PID file \"%s\": %s",
                pidfile, strerror( errno ) );
        closelog();
        close( mq );
        free( mqt );
        return rc;
    }

    sigemptyset( &sig_set );
    sigaddset( &sig_set, SIGCHLD );
    sigaddset( &sig_set, SIGTSTP );
    sigaddset( &sig_set, SIGTTOU );
    sigaddset( &sig_set, SIGTTIN );
    sigprocmask( SIG_BLOCK, &sig_set, NULL );
    mq_sig_init( SIGTERM );
    mq_sig_init( SIGINT );
    mq_sig_init( SIGHUP );
    mq_sig_init( SIGUSR1 );
    mq_sig_init( SIGUSR2 );

    while( !terminate ) {
        int fout;
        size_t readed;
        sleep( ( unsigned int )sleep_seconds );
        syslog( LOG_INFO, "Start reading...\n" );
        readed = read( mq, mqt, sizeof( size_t ) + MQ_MSG_LIMIT );

        if( ( int )readed == -1 ) {
            syslog( LOG_ERR, "Can not read from \"%s\": %s\n",
                    procfile, strerror( errno ) );
            continue;
        }

        if( readed < sizeof( mq_transport ) ) {
            syslog( LOG_ERR, "No enough data readed from \"%s\" (%zu bytes)\n", procfile,
                    readed );
            continue;
        }

        fout = open( outfile, O_WRONLY | O_CREAT | O_APPEND, S_IWUSR | S_IRUSR );

        if( fout == -1 ) {
            syslog( LOG_ERR, "Can not open \"%s\": %s\n", outfile, strerror( errno ) );
            continue;
        }

        if( write( fout, mqt, readed ) != ( int )readed ) {
            syslog( LOG_ERR, "Can not write \"%s\": %s\n", outfile, strerror( errno ) );
        }

        syslog( LOG_INFO, "Message stored OK (%zu bytes)\n", readed );
        close( fout );
    }

    closelog();
    close( mq );
    free( mqt );
    unlink( pidfile );
    return rc;
}

