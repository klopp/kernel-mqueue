#ifndef PIDFILE_H_
# define PIDFILE_H_

#include <stdio.h>
#include <unistd.h>
#include <sys/stat.h>
#include <sys/file.h>
#include <string.h>
#include <errno.h>
#include <signal.h>
#include <fcntl.h>

int read_pid( const char *pidfile );
int check_pid( const char *pidfile );
int write_pid( const char *pidfile );

#endif
