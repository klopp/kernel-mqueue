#ifndef _KERNEL_H_
# define _KERNEL_H_

#if defined(__KERNEL__)
# include <linux/version.h>
# include <linux/init.h>
# include <linux/module.h>
# include <linux/proc_fs.h>
# include <linux/mutex.h>
# include <linux/kfifo.h>
# include <linux/slab.h>
# include <linux/vmalloc.h>
# include <linux/time.h>
# include <asm/uaccess.h>
# if LINUX_VERSION_CODE < KERNEL_VERSION(3,10,0)
#  define proc_remove( pde ) \
            remove_proc_entry( (pde)->name, NULL )
struct proc_dir_entry *proc_create( const char *name,
                                    mode_t mode,
                                    struct proc_dir_entry *parent,
                                    const struct file_operations *proc_fops );
# endif
#else
# include <sys/types.h>
# include <unistd.h>
# include <errno.h>
# include <stdio.h>
# include <string.h>
# include <stdlib.h>
# include <signal.h>
# include <limits.h>
# include <syslog.h>
# include <sys/stat.h>
# include <fcntl.h>
/*
 *
 */
# define THIS_MODULE            NULL
# define printk                 printf
# define KERN_ERR
# define KERN_INFO

# define S_IRUGO                (S_IRGRP|S_IRUSR|S_IROTH)
# define S_IWUGO                (S_IWGRP|S_IWUSR|S_IWOTH)

# define DECLARE_KFIFO(x,y,z)   y x[z]
# define INIT_KFIFO(x)

# define __init
# define __exit

# define module_init(x)
# define module_exit(x)
# define module_param(x,y,z)

# define MODULE_LICENSE(x)
# define MODULE_AUTHOR(x)
# define MODULE_VERSION(x)
# define MODULE_DESCRIPTION(x)
# define EXPORT_SYMBOL(x)

#endif

#endif
