#include "mq.h"

/* -----------------------------------------------------------------------------
 * Print usage and exit:
 ---------------------------------------------------------------------------- */
void usage( const char *self, const char *msg ) {
    fprintf( stderr, "\nUsage: %s %s\n", self, msg );
    exit( EINVAL );
}

/* -----------------------------------------------------------------------------
 * Print error message and return errno:
 ---------------------------------------------------------------------------- */
int perr( const char *prefix, const char *msg ) {
    int rc = errno;
    fprintf( stderr, "\nERROR, " );

    if( msg ) {
        fprintf( stderr, "%s", prefix );
        perror( msg );
    }
    else {
        perror( prefix );
    }

    return rc;
}

